from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = _('Категории товаров')
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('product_list_by_category', args=[self.slug])


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=255, verbose_name=_('Название товара'))
    url = models.SlugField(max_length=160, unique=True, blank=True, null=True)
    firm = models.CharField(max_length=255, verbose_name=_('Производитель'))
    image = models.ImageField(verbose_name=_('Изображение'))
    price = models.DecimalField(default=0.0, max_digits=10, decimal_places=2, verbose_name=_('Цена в BYN'))
    description = models.TextField(blank=True, null=True, verbose_name=_('Описание товара'))

    def get_absolute_url(self):
        return reverse('product_detail', kwargs={'slug': self.url})

    class Meta:
        ordering = ('name',)
        index_together = (('id', 'url'),)

    def __str__(self):
        return self.name

