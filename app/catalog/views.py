from django.shortcuts import render, get_object_or_404
from .filters import ProductFilter
from .models import Product, Category
from cart.forms import CartAddProductForm


def product(request, category_slug=None):
    """ Список всех моделей """
    category = None
    products = Product.objects.all()
    filter = ProductFilter(request.GET, queryset=Product.objects.all())
    products = filter.qs
    categories = Category.objects.all()
    cart_product_form = CartAddProductForm()


    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)

    return render(request, 'catalog/product.html', {'category': category, 'categories': categories,
                                                    'products': products, 'filter': filter,
                                                    'cart_product_form': cart_product_form,})



def product_detail(request, slug):
    """ Подробное описание определенной модели ламината """
    product = Product.objects.get(url=slug)
    cart_product_form = CartAddProductForm()
    return render(request, 'catalog/product_detail.html', {'product': product, 'cart_product_form': cart_product_form})

