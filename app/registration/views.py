from django.shortcuts import render, redirect
from django.contrib.auth import login, logout
from django.contrib import messages
from .forms import UserLoginForm, ProfileForm


def register(request):
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, 'Вы успешно зарегистрировались!')
            return redirect('home')
        else:
            messages.error(request, 'Ошибка регистрации:(')

    else:
        form = ProfileForm()

    return render(request, 'registration/register.html', {"form":form})


def user_login (request):
    if request.method=='POST':
        form = UserLoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            messages.success(request, '')
            return redirect('home')
    else:
        form = UserLoginForm()

    return render(request, 'registration/login.html', {'form':form})


def user_logout(request):
    logout(request)
    return redirect ('login')

