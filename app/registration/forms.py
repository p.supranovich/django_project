from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from registration.models import Profile
from phonenumber_field.widgets import PhoneNumberInternationalFallbackWidget


class UserLoginForm(AuthenticationForm):
    username = forms.CharField(label=_('Имя пользователя'),
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label=_('Пароль'),
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}))


class ProfileForm(UserCreationForm):
    password1 = forms.CharField(label=_('Пароль'),
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=_('Подтверждение пароля'),
                                widget=forms.PasswordInput)

    class Meta:
        model = Profile
        fields = ('username', 'first_name', 'last_name', 'email', 'phone')

        widgets = {
                      'phone': PhoneNumberInternationalFallbackWidget(attrs={
                          'placeholder': _('Телефон'),
                          'class': 'form-control',
                      }),
                  },

