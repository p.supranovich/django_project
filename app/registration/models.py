from django.db import models
from django.contrib.auth.models import User
from phonenumber_field.modelfields import PhoneNumberField


class Profile(User):
    phone = PhoneNumberField(verbose_name='Телефон')



