"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from core.views import home, about_us, buy, recommendation, contacts, add_comment, change_theme
from django.conf.urls.static import static
from django.conf import settings
# from django.utils.translation import gettext_lazy as _
from django.conf.urls.i18n import i18n_patterns


urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('home/', include('registration.urls')),
    path('catalog/', include('catalog.urls')),
    path('about_us/', about_us, name='about_us'),
    path('buy/', buy, name='buy'),
    path('recommendation/', recommendation, name='recommendation'),
    path('contacts/', contacts, name='contacts'),
    path('recommendation/add_comment', add_comment, name='add_comment'),
    path('change_theme', change_theme, name='change_theme'),
    path('cart/', include('cart.urls')),
    path('orders/', include('orders.urls')),
    path('', home, name='home'),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# urlpatterns = i18n_patterns(
#     path('admin/', admin.site.urls),
#     path(_(''), home, name='home'),
#     path(_('home/'), include('registration.urls')),
#     path(_('catalog/'), include('catalog.urls')),
#     path(_('about_us/'), about_us, name='about_us'),
#     path(_('buy/'), buy, name='buy'),
#     path(_('recommendation/'), recommendation, name='recommendation'),
#     path(_('contacts/'), contacts, name='contacts'),
#     path(_('recommendation/add_comment'), add_comment, name='add_comment'),
#     path(_('change_theme'), change_theme, name='change_theme'),
#     path(_('cart/'), include('cart.urls')),
#     path(_('orders/'), include('orders.urls')),
# ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

