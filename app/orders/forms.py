from django import forms
from django.forms import TextInput, EmailInput
from .models import Order
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.widgets import PhoneNumberInternationalFallbackWidget


class OrderCreateForm(forms.ModelForm):
    """ Форма для создания заказа на сайте """
    class Meta:
        model = Order
        fields = ['first_name', 'last_name', 'email', 'phone', 'address', 'city']

        widgets = {
            'first_name': TextInput(attrs={
                'placeholder': _('Имя'),
            }),
            'last_name': TextInput(attrs={
                'placeholder': _('Фамилия')
            }),
            'email': EmailInput(attrs={
                'placeholder': _('Адрес электронной почты')
            }),
            'phone': PhoneNumberInternationalFallbackWidget(attrs={
              'placeholder':_('Телефон'),
              'class': 'form-control',
            }),
            'address': TextInput(attrs={
                'placeholder': _('Адрес')
            }),
            'city': TextInput(attrs={
                'placeholder': _('Город')
            }),
        },

