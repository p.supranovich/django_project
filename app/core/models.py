from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

User = get_user_model()


class Application(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Имя'))
    surname = models.CharField(max_length=255, verbose_name=_('Фамилия'))
    phone = PhoneNumberField(verbose_name=_('Телефон'))
    comment = models.TextField(max_length=255, verbose_name=_('Комментарий'))
    created = models.DateTimeField(default=timezone.now, verbose_name=_('Дата создания'))

    def __str__(self):
        return f'{self.name}, {self.surname}'

    class Meta:
        verbose_name = _('Форма')
        verbose_name_plural = _('Формы')
        ordering = ['-created']


class Recommend(models.Model):
    title = models.CharField(_('Заголовок'), max_length=100,)
    name = models.CharField(_('Имя автора'), max_length=255,)
    comment = models.TextField(_('Комментарий'), max_length=255,)
    created = models.DateTimeField(default=timezone.now,)

    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name = _('Отзыв')
        verbose_name_plural = _('Отзывы')
        ordering = ['-created']
