from django.contrib import admin
from .models import Application, Recommend


class ApplicationAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'surname', 'phone', 'comment', 'created')
    list_display_links =('id', 'name',)
    search_fields = ('name', 'surname', 'phone', 'comment')


class RecommendAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'comment', 'name', 'created')
    list_display_links = ('id', 'title',)
    search_fields = ('title', 'comment', 'name')


admin.site.register(Application, ApplicationAdmin)
admin.site.register(Recommend, RecommendAdmin)

