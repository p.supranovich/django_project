
Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>',
});

Vue.component('animal', {
    props: ['species', 'created'],
    template: '<div class="carousel-item active"><img src="{{ img }}" class="d-block w-100" alt="{{ created }}"></div>',
});


var app = new Vue({
    el: '#app',
    delimiters: ['<%', '%>'],
    data: {
        message: 'Привет, Vue!',
    }
});

var app4 = new Vue({
    el: '#app4',
    delimiters: ['<%', '%>'],
    data: {
         todos: [
             { text: 'Изучить JavaScript' },
             { text: 'Изучить Vue' },
             { text: 'Создать что-нибудь классное' },
         ]
    }
});


 var app6 = new Vue({
     el: '#app-6',
     delimiters: ['<%', '%>'],
     data: {
        message: 'Привет, Vue!',
        classes: ''
    }
});

var type="text/javascript">
$(document).ready(function(){
  $(".dropdown-toggle-js").dropdown();
});