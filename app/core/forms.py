from .models import Application, Recommend
from django.forms import ModelForm, TextInput, Textarea
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.widgets import PhoneNumberInternationalFallbackWidget



class ApplicationForm(ModelForm):
    class Meta:
        model = Application
        fields = ['name', 'surname', 'phone', 'comment']

        widgets = {
            'name': TextInput(attrs={
                'placeholder': _('Имя'),
                'class': 'form-control',
            }),
            'surname': TextInput(attrs={
                'placeholder': _('Фамилия'),
                'class': 'form-control',
            }),
            'phone': PhoneNumberInternationalFallbackWidget(attrs={
                'placeholder': _('Телефон'),
                'class': 'form-control',
            }),
            'comment': Textarea(attrs={
                'placeholder': _('Комментарий'),
                'class': 'form-control',
                'rows': 5,
            })
        }


class RecommendForm(ModelForm):
    class Meta:
        model = Recommend
        fields = ['title', 'name', 'comment']

        widgets = {
            'title': TextInput(attrs={
                'class': 'form-control',
                'placeholder': _('Заголовок'),
            }),
            'name': TextInput(attrs={
                'class': 'form-control',
                'placeholder': _('Ваше имя')
            }),
            'comment': Textarea(attrs={
                'class': 'form-control',
                'placeholder': _('Комментарий')
            })
        }


