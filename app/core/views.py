from django.shortcuts import render, redirect
from django.template import loader
from .forms import RecommendForm
from .models import Recommend
from core.forms import ApplicationForm
from django.core.mail import send_mail
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect


def home(request):
    template = loader.get_template('home.html')
    return HttpResponse(template.render({}, request))


def about_us(request):
    template = loader.get_template('about_us.html')
    return HttpResponse(template.render({}, request))


def recommendation(request):
    comments = Recommend.objects.order_by('-created')
    return render(request, 'recommends.html', {'comments': comments})


def add_comment(request):
    error = ''
    if request.method == 'POST':
        form = RecommendForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('recommendation')
        else:
            error = 'Форма заполнена неверно'

    form = RecommendForm()

    data = {
        'form': form,
        'error': error,
    }
    return render(request, 'add_comment.html', data)


def contacts(request):
    template = loader.get_template('contacts.html')
    return HttpResponse(template.render({}, request))


def buy(request):

    """ Страница с формой заявки на консультацию """

    if request.method =='POST':
        form = ApplicationForm(data=request.POST)
        if form.is_valid():
            message = 'Пользователь ' + str(form.cleaned_data['surname']) + ' ' + str(form.cleaned_data['name'])+\
                      ' \n Просит Вас перезвонить. \n Номер телефона: '+ str(form.cleaned_data['phone'])+\
                      '\n Коммантарий: \n'+str(form.cleaned_data['comment'])
            mail = send_mail('Обратный звонок!', message,
                            'polia1827@gmail.com', ['supranovich.polina@yandex.ru'], fail_silently = True)
            form.save()

            if mail:
                messages.success(request, 'Письмо отправлено')
                return redirect('buy')
            else:
                messages.error(request, 'Ошибка отправки')
        else:
            error = 'Форма заполнена неверно'
    else:
        form = ApplicationForm()

    context = {
        'form': form,
    }

    return render(request, 'application_form.html', context)


def change_theme(request):
    theme = request.POST.get('theme')
    referrer = request.META['HTTP_REFERER']
    request.session['theme'] = theme
    return redirect(referrer)

