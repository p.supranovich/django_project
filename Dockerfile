FROM python:3.9

RUN apt-get install libpq-dev

RUN apt-get update
RUN apt-get update && apt-get install -y gettext libgettextpo-dev

RUN mkdir /app
COPY ./app/requirements.txt /app/

RUN pip install -r /app/requirements.txt

WORKDIR /app
